import React, { Component } from 'react';
import './style.css';
import CustomModal from '../../components/Modal/Modal';
import * as API from '../../services/api/api';
class ViewBranch extends Component {
  state = {
    branchList: [],
    modalIsOpen: false,
    currentBranch: {},
    currentIndex: -1
  };
  async componentWillMount() {
    const data = await API.fetchGetBranchList();
    //console.log(data);
    if (data.status === 200) {
      const dataJson = await data.json();
      //console.log(dataJson);
      this.setState({ ...this.state, branchList: dataJson });
    }
    //dummies
    // this.setState({
    //   ...this.state,
    //   branchList: [
    //     { id: 1, name: 'Villa', address: 'HatYai' },
    //     { id: 2, name: 'Kanjana', address: 'Krung Tep' },
    //     { id: 3, name: 'Gandhibag', address: 'Smut Prakarn' },
    //     { id: 4, name: 'Itwari', address: 'Songkla' },
    //     { id: 5, name: 'Chopasni', address: 'Insri' }
    //   ]
    // });
  }

  //onEditBranch -> onEditBranchOK (Click OK to edit)
  onEditBranch(index) {
    //To Show only
    // console.log(this.state.branchList[index]);

    this.setState({
      ...this.state,
      currentBranch: this.state.branchList[index],
      modalIsOpen: true,
      currentIndex: index
    });
  }

  onEditBranchOK = async () => {
    //1) edit in database
    //2) edit in state
    const data = await API.fetchPutBranch(
      this.state.currentBranch,
      this.state.currentBranch['id']
    );
    if (data.status === 200) {
      alert(`successfully edited -> id: ${this.state.currentBranch['id']}`);
      let newBranchList = this.state.branchList.slice();
      newBranchList[this.state.currentIndex] = this.state.currentBranch;
      this.setState({
        ...this.state,
        branchList: newBranchList,
        modalIsOpen: false
      });
    } else {
      alert(`Unable to edit -> id:  ${this.state.currentBranch['id']}`);
    }
  };

  async onDeleteBranch(index, branchId) {
    //1) delete in database
    //2) delete in state
    const data = await API.fetchDeleteBranch(branchId);
    if (data.status === 200) {
      alert(`successfully deleted -> id: ${branchId}`);
      let newBranchList = this.state.branchList.slice(); //deep copy
      newBranchList.splice(index, 1);
      this.setState({ ...this.state, branchList: newBranchList });
    } else {
      alert(`Unable to delete -> id: ${branchId}`);
    }
  }

  render() {
    const { branchList } = this.state;

    return (
      <>
        <CustomModal
          isOpen={this.state.modalIsOpen}
          onRequestClose={() => {
            this.setState({ ...this.state, modalIsOpen: false });
          }}
        >
          <form>
            <h1 className='HeaderView'>Edit Branch</h1>

            <div className='Custom_Container'>
              <div className='row'>
                <div className='col-5'>
                  <label>Branch name: </label>
                </div>
                <div className='col-6'>
                  <input
                    className='form-control'
                    type='text'
                    name='branch_name'
                    value={this.state.currentBranch.name || ''}
                    onChange={event => {
                      this.setState({
                        ...this.state,
                        currentBranch: {
                          ...this.state.currentBranch,
                          name: event.target.value
                        }
                      });
                    }}
                  />
                </div>
              </div>
              <div className='row'>
                <div className='col-5'>
                  <label>Address: </label>
                </div>
                <div className='col-6'>
                  <input
                    className='form-control'
                    type='text'
                    name='address'
                    value={this.state.currentBranch.address || ''}
                    onChange={event => {
                      this.setState({
                        ...this.state,
                        currentBranch: {
                          ...this.state.currentBranch,
                          address: event.target.value
                        }
                      });
                    }}
                  />
                </div>
              </div>

              <button
                onClick={this.onEditBranchOK}
                className='btn mx-1 OK-btn'
                type='button'
              >
                OK
              </button>
              <button
                onClick={() => {
                  this.setState({ ...this.state, modalIsOpen: false });
                }}
                className='btn mx-1 Cancel-btn'
                type='button'
              >
                Cancel
              </button>
            </div>
          </form>
        </CustomModal>
        <h1 className='HeaderView'>List of Branches</h1>
        <table>
          <tbody>
            <tr>
              <th>Id</th>
              <th>Name</th>
              <th>Address</th>
            </tr>
            {branchList.length > 0 &&
              branchList.map((element, index) => (
                <tr key={element['id']}>
                  <td>{element['id']}</td>
                  <td>{element['name']}</td>
                  <td>{element['address']}</td>
                  <td className='Transparent'>
                    <button
                      className='mx-1 btn Edit-btn'
                      onClick={() => {
                        this.onEditBranch(index);
                      }}
                    >
                      EDIT
                    </button>
                  </td>
                  <td className='Transparent'>
                    <button
                      className='mx-1 btn Delete-btn'
                      onClick={() => {
                        this.onDeleteBranch(index, element['id']);
                      }}
                    >
                      DELETE
                    </button>
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
      </>
    );
  }
}

export default ViewBranch;
