package com.g_able.exam.spring.template.app.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import com.g_able.exam.spring.template.app.compositekey.BranchProductCompositeKey;

@Entity
@Table(name = "branch_product")
@IdClass(BranchProductCompositeKey.class)
public class BranchProduct {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private long id;

    @Column(name = "branch_id", nullable = false)
    private long branchId;

    @Column(name = "product_id", nullable = false)
    private long productId;

    @Column(name = "quantity", nullable = false)
    private long quantity;

    public BranchProduct() {
    }

    public BranchProduct(long id, long branchId, long productId, long quantity) {
        this.id = id;
        this.branchId = branchId;
        this.productId = productId;
        this.quantity = quantity;
    }

    public BranchProduct(long branchId, long productId, long quantity) {
        this.branchId = branchId;
        this.productId = productId;
        this.quantity = quantity;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getBranchId() {
        return this.branchId;
    }

    public void setBranchId(long branchId) {
        this.branchId = branchId;
    }

    public long getProductId() {
        return this.productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public long getQuantity() {
        return this.quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

}