package com.g_able.exam.spring.template.app.service;

import java.util.List;

import javax.transaction.Transactional;

import com.g_able.exam.spring.template.app.repository.BranchRepository;
import com.g_able.exam.spring.template.app.entity.Branch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class BranchService {
    @Autowired
    private BranchRepository branchRepository;

    public void persistBranch(Branch branchEntity) {
        branchRepository.save(branchEntity);
    }

    public List<Branch> getBranchList() {
        return (List<Branch>) branchRepository.findAll();
    }

    public void deleteBranchbyId(long id) {
        branchRepository.deleteById(id);
    }
}