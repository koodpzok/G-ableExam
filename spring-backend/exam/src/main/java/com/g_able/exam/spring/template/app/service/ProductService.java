package com.g_able.exam.spring.template.app.service;

import java.util.List;

import javax.transaction.Transactional;

import com.g_able.exam.spring.template.app.repository.ProductRepository;
import com.g_able.exam.spring.template.app.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class ProductService {
    @Autowired
    private ProductRepository productRepository;

    // persist and update
    public void persistProduct(Product productEntity) {
        productRepository.save(productEntity);
    }

    public List<Product> getProductList() {
        return (List<Product>) productRepository.findAll();
    }

    public void deleteProductbyId(long id) {
        productRepository.deleteById(id);
    }
}