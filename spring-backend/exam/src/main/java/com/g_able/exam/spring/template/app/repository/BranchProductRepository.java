package com.g_able.exam.spring.template.app.repository;

import org.springframework.data.repository.CrudRepository;
import com.g_able.exam.spring.template.app.entity.BranchProduct;
import com.g_able.exam.spring.template.app.compositekey.BranchProductCompositeKey;

public interface BranchProductRepository extends CrudRepository<BranchProduct, BranchProductCompositeKey> {
    public void deleteById(long id);
}