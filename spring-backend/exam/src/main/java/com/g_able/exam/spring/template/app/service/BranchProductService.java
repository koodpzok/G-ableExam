package com.g_able.exam.spring.template.app.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import com.g_able.exam.spring.template.app.repository.BranchProductRepository;
import com.g_able.exam.spring.template.app.entity.BranchProduct;
import com.g_able.exam.spring.template.app.compositekey.BranchProductCompositeKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class BranchProductService {
    @Autowired
    private BranchProductRepository branchProductRepository;

    public boolean persistBranchProduct(BranchProduct branchProductEntity) {

        Optional<BranchProduct> isFound = branchProductRepository.findById(
                new BranchProductCompositeKey(branchProductEntity.getBranchId(), branchProductEntity.getProductId()));

        if (isFound.isPresent()) {
            return false;
        }

        branchProductRepository.save(branchProductEntity);
        return true;
    }

    public List<BranchProduct> getBranchProductList() {
        return (List<BranchProduct>) branchProductRepository.findAll();
    }

    public void deleteBranchProductbyId(long id) {
        branchProductRepository.deleteById(id);
    }
}