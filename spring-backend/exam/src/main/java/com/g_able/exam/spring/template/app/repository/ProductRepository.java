package com.g_able.exam.spring.template.app.repository;

import org.springframework.data.repository.CrudRepository;
import com.g_able.exam.spring.template.app.entity.Product;

public interface ProductRepository extends CrudRepository<Product, Long> {

}