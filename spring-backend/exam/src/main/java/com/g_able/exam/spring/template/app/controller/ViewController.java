// GET, PUT, DELETE
package com.g_able.exam.spring.template.app.controller;

import com.g_able.exam.spring.template.app.service.ProductService;
import com.g_able.exam.spring.template.app.service.BranchService;
import com.g_able.exam.spring.template.app.service.BranchProductService;

import com.g_able.exam.spring.template.app.entity.Product;
import com.g_able.exam.spring.template.app.entity.Branch;
import com.g_able.exam.spring.template.app.entity.BranchProduct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class ViewController {
    @Autowired
    private ProductService productService;
    @Autowired
    private BranchService branchService;
    @Autowired
    private BranchProductService branchProductService;

    // GET
    @GetMapping("/viewProduct")
    public List<Product> productList() {
        return productService.getProductList();
    }

    @GetMapping("/viewBranch")
    public List<Branch> branchList() {
        return branchService.getBranchList();
    }

    @GetMapping("/viewProductBranch")
    public List<BranchProduct> branchProductList() {
        return branchProductService.getBranchProductList();
    }

    // PUT
    @PutMapping("/product/{id}")
    public ResponseEntity<Product> editProduct(@PathVariable long id, @RequestBody Product p) {
        Product product = new Product(id, p.getName(), p.getDetail(), p.getPrice());
        productService.persistProduct(product);
        return new ResponseEntity<Product>(product, HttpStatus.OK);

    }

    @PutMapping("/branch/{id}")
    public ResponseEntity<Branch> editBranch(@PathVariable long id, @RequestBody Branch b) {
        Branch branch = new Branch(id, b.getName(), b.getAddress());
        branchService.persistBranch(branch);
        return new ResponseEntity<Branch>(branch, HttpStatus.OK);

    }

    @PutMapping("/productBranch/{id}")
    public ResponseEntity<BranchProduct> editProductBranch(@PathVariable long id, @RequestBody BranchProduct bp) {
        BranchProduct branchProduct = new BranchProduct(id, bp.getBranchId(), bp.getProductId(), bp.getQuantity());
        branchProductService.persistBranchProduct(branchProduct);
        return new ResponseEntity<BranchProduct>(branchProduct, HttpStatus.OK);

    }

    // DELETE
    @DeleteMapping("/product/{id}")
    public ResponseEntity<Long> deleteProduct(@PathVariable long id) {
        productService.deleteProductbyId(id);
        return new ResponseEntity<Long>(id, HttpStatus.OK);
    }

    @DeleteMapping("/branch/{id}")
    public ResponseEntity<Long> deleteBranch(@PathVariable long id) {
        branchService.deleteBranchbyId(id);
        return new ResponseEntity<Long>(id, HttpStatus.OK);
    }

    @DeleteMapping("/productBranch/{id}")
    public ResponseEntity<Long> deleteProductBranch(@PathVariable long id) {
        branchProductService.deleteBranchProductbyId(id);
        return new ResponseEntity<Long>(id, HttpStatus.OK);
    }
}