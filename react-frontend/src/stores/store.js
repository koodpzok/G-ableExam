import { observable, action, computed, decorate } from 'mobx';
// where we keep state for inter-component interactions
class Store {
  a1 = 1;
  a2 = 2;
}
decorate(Store, {
  a1: observable,
  a2: observable
});

export default new Store();
