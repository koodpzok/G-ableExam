package com.g_able.exam.spring.template.app.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "branch")
public class Branch {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private long id;

    @Column(name = "name", length = 45, nullable = false)
    private String name;

    @Column(name = "address", length = 128, nullable = false)
    private String address;

    public Branch() {
    }

    public Branch(long id, String name, String address) {
        this.id = id;
        this.name = name;
        this.address = address;

    }

    public Branch(String name, String address) {
        this.name = name;
        this.address = address;

    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

}