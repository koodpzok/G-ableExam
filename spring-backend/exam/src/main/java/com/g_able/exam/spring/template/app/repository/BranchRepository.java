package com.g_able.exam.spring.template.app.repository;

import org.springframework.data.repository.CrudRepository;
import com.g_able.exam.spring.template.app.entity.Branch;

public interface BranchRepository extends CrudRepository<Branch, Long> {

}