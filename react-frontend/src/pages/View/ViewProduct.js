import React, { Component } from 'react';
import './style.css';
import CustomModal from '../../components/Modal/Modal';
import * as API from '../../services/api/api';

class ViewProduct extends Component {
  state = {
    productList: [],
    modalIsOpen: false,
    currentProduct: {},
    currentIndex: -1
  };

  async componentWillMount() {
    const data = await API.fetchGetProductList();
    if (data.status === 200) {
      const dataJson = await data.json();
      //console.log(dataJson);
      this.setState({ ...this.state, productList: dataJson });
    }
    //dummies
    // this.setState({
    //   ...this.state,
    //   productList: [
    //     { id: 1, name: 'Broccoli', detail: 'Broccoli good good', price: 9.33 },
    //     { id: 2, name: 'Cabbage', detail: 'Cabbage good good', price: 11.11 },
    //     { id: 3, name: 'Ribbon', detail: 'Ribbon good good', price: 43.21 },
    //     { id: 4, name: 'TV', detail: 'TV good good', price: 2.93 },
    //     { id: 5, name: 'Ring', detail: 'Ring good good', price: 239.11 }
    //   ]
    // });
  }

  //onEditProduct -> onEditProductOK (Click OK to edit)
  onEditProduct(index) {
    //To Show only
    // console.log(this.state.productList[index]);

    this.setState({
      ...this.state,
      currentProduct: this.state.productList[index],
      modalIsOpen: true,
      currentIndex: index
    });
  }

  onEditProductOK = async () => {
    //1) edit in database
    //2) edit in state
    const data = await API.fetchPutProduct(
      this.state.currentProduct,
      this.state.currentProduct['id']
    );
    if (data.status === 200) {
      alert(`successfully edited -> id: ${this.state.currentProduct['id']}`);
      let newProductList = this.state.productList.slice();
      newProductList[this.state.currentIndex] = this.state.currentProduct;
      this.setState({
        ...this.state,
        productList: newProductList,
        modalIsOpen: false
      });
    } else {
      alert(`Unable to edit -> id: ${this.state.currentProduct['id']}`);
    }
  };

  async onDeleteProduct(index, productId) {
    //1) delete in database
    //2) delete in state
    const data = await API.fetchDeleteProduct(productId);
    if (data.status === 200) {
      alert(`successfully deleted -> id: ${productId}`);
      let newProductList = this.state.productList.slice(); //deep copy
      newProductList.splice(index, 1);
      this.setState({ ...this.state, productList: newProductList });
    } else {
      alert(`Unable to delete -> id: ${productId}`);
    }
  }

  render() {
    const { productList } = this.state;

    return (
      <>
        <CustomModal
          isOpen={this.state.modalIsOpen}
          onRequestClose={() => {
            this.setState({ ...this.state, modalIsOpen: false });
          }}
        >
          <form>
            <h1 className='HeaderView'>Edit Product</h1>

            <div className='Custom_Container'>
              <div className='row'>
                <div className='col-5'>
                  <label>Product name: </label>
                </div>
                <div className='col-6'>
                  <input
                    className='form-control '
                    type='text'
                    name='product_name'
                    value={this.state.currentProduct.name || ''}
                    onChange={event => {
                      this.setState({
                        ...this.state,
                        currentProduct: {
                          ...this.state.currentProduct,
                          name: event.target.value
                        }
                      });
                    }}
                  />
                </div>
              </div>
              <div className='row'>
                <div className='col-5'>
                  <label>Detail: </label>
                </div>
                <div className='col-6'>
                  <input
                    className='form-control'
                    type='text'
                    name='detail'
                    value={this.state.currentProduct.detail || ''}
                    onChange={event => {
                      this.setState({
                        ...this.state,
                        currentProduct: {
                          ...this.state.currentProduct,
                          detail: event.target.value
                        }
                      });
                    }}
                  />
                </div>
              </div>
              <div className='row'>
                <div className='col-5'>
                  <label>Price: </label>
                </div>
                <div className='col-6'>
                  <input
                    className='form-control'
                    type='number'
                    step='.01'
                    name='price'
                    value={this.state.currentProduct.price || ''}
                    onChange={event => {
                      this.setState({
                        ...this.state,
                        currentProduct: {
                          ...this.state.currentProduct,
                          price: event.target.value
                        }
                      });
                    }}
                  />
                </div>
              </div>

              <button
                onClick={this.onEditProductOK}
                className='btn mx-1 OK-btn'
                type='button'
              >
                OK
              </button>
              <button
                onClick={() => {
                  this.setState({ ...this.state, modalIsOpen: false });
                }}
                className='btn mx-1 Cancel-btn'
                type='button'
              >
                Cancel
              </button>
            </div>
          </form>
        </CustomModal>
        <h1 className='HeaderView'>List of Products</h1>
        <table>
          <tbody>
            <tr>
              <th>Id</th>
              <th>Name</th>
              <th>Detail</th>
              <th>Price</th>
            </tr>
            {productList.length > 0 &&
              productList.map((element, index) => (
                <tr key={element['id']}>
                  <td>{element['id']}</td>
                  <td>{element['name']}</td>
                  <td>{element['detail']}</td>
                  <td>{element['price']}</td>
                  <td className='Transparent'>
                    <button
                      className='mx-1 btn Edit-btn'
                      onClick={() => {
                        this.onEditProduct(index);
                      }}
                    >
                      EDIT
                    </button>
                  </td>
                  <td className='Transparent'>
                    <button
                      className='mx-1 btn Delete-btn'
                      onClick={() => {
                        this.onDeleteProduct(index, element['id']);
                      }}
                    >
                      DELETE
                    </button>
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
      </>
    );
  }
}

export default ViewProduct;
