import React, { Component } from 'react';
import './style.css';
import CustomModal from '../../components/Modal/Modal';
import * as API from '../../services/api/api';
class ViewProductBranch extends Component {
  state = {
    productBranchList: [],
    modalIsOpen: false,
    currentProductBranch: {},
    currentIndex: -1
  };
  async componentWillMount() {
    const data = await API.fetchGetProductBranchList();
    //console.log(data);
    if (data.status === 200) {
      const dataJson = await data.json();
      //console.log(dataJson);
      this.setState({ ...this.state, productBranchList: dataJson });
    }
    // this.setState({
    //   ...this.state,
    //   productBranchList: [
    //     { id: 1, branchId: 1, productId: 2, quantity: 5 },
    //     { id: 2, branchId: 2, productId: 3, quantity: 1 },
    //     { id: 3, branchId: 3, productId: 4, quantity: 3 },
    //     { id: 4, branchId: 4, productId: 2, quantity: 11 },
    //     { id: 5, branchId: 5, productId: 3, quantity: 7 }
    //   ]
    // });
  }

  //onEditProductBranch -> onEditProductBranchOK (Click OK to edit)
  onEditProductBranch(index) {
    //To Show only
    // console.log(this.state.productBranchList[index]);

    this.setState({
      ...this.state,
      currentProductBranch: this.state.productBranchList[index],
      modalIsOpen: true,
      currentIndex: index
    });
  }

  onEditProductBranchOK = async () => {
    //1) edit in database
    //2) edit in state
    const data = await API.fetchPutProductBranch(
      this.state.currentProductBranch,
      this.state.currentProductBranch['id']
    );
    if (data.status === 200) {
      alert(
        `successfully edited -> id: ${this.state.currentProductBranch['id']}`
      );
      let newProductBranchList = this.state.productBranchList.slice();
      newProductBranchList[
        this.state.currentIndex
      ] = this.state.currentProductBranch;
      this.setState({
        ...this.state,
        productBranchList: newProductBranchList,
        modalIsOpen: false
      });
    } else {
      alert(`Unable to edit -> id: ${this.state.currentProductBranch['id']}`);
    }
  };

  async onDeleteProductBranch(index, id) {
    //1) delete in database
    //2) delete in state
    const data = await API.fetchDeleteBranchProduct(id);
    if (data.status === 200) {
      alert(`successfully deleted -> id: ${id}`);
      let newProductBranchList = this.state.productBranchList.slice(); //deep copy
      newProductBranchList.splice(index, 1);
      this.setState({ ...this.state, productBranchList: newProductBranchList });
    } else {
      alert(`Unable to delete -> id: ${id}`);
    }
  }

  render() {
    const { productBranchList } = this.state;
    return (
      <>
        <CustomModal
          isOpen={this.state.modalIsOpen}
          onRequestClose={() => {
            this.setState({ ...this.state, modalIsOpen: false });
          }}
        >
          <form>
            <h1 className='HeaderView'>Edit ProductBranch</h1>

            <div className='Custom_Container'>
              <div className='row'>
                <div className='col-5'>
                  <label>Branch id: </label>
                </div>
                <div className='col-6'>
                  <input
                    className='form-control'
                    type='text'
                    name='branch_id'
                    value={this.state.currentProductBranch.branchId || ''}
                    onChange={event => {
                      this.setState({
                        ...this.state,
                        currentProductBranch: {
                          ...this.state.currentProductBranch,
                          branchId: event.target.value
                        }
                      });
                    }}
                    disabled
                  />
                </div>
              </div>
              <div className='row'>
                <div className='col-5'>
                  <label>Product id: </label>
                </div>
                <div className='col-6'>
                  <input
                    className='form-control'
                    type='text'
                    name='product_id'
                    value={this.state.currentProductBranch.productId || ''}
                    onChange={event => {
                      this.setState({
                        ...this.state,
                        currentProductBranch: {
                          ...this.state.currentProductBranch,
                          productId: event.target.value
                        }
                      });
                    }}
                    disabled
                  />
                </div>
              </div>
              <div className='row'>
                <div className='col-5'>
                  <label>Quantity: </label>
                </div>
                <div className='col-6'>
                  <input
                    className='form-control'
                    type='number'
                    step='.01'
                    name='quantity'
                    value={this.state.currentProductBranch.quantity || ''}
                    onChange={event => {
                      this.setState({
                        ...this.state,
                        currentProductBranch: {
                          ...this.state.currentProductBranch,
                          quantity: event.target.value
                        }
                      });
                    }}
                  />
                </div>
              </div>

              <button
                onClick={this.onEditProductBranchOK}
                className='btn mx-1 OK-btn'
                type='button'
              >
                OK
              </button>
              <button
                onClick={() => {
                  this.setState({ ...this.state, modalIsOpen: false });
                }}
                className='btn mx-1 Cancel-btn'
                type='button'
              >
                Cancel
              </button>
            </div>
          </form>
        </CustomModal>
        <h1 className='HeaderView'>List of Products in Each Branch</h1>
        <table>
          <tbody>
            <tr>
              <th>Id</th>
              <th>BranchId</th>
              <th>ProductId</th>
              <th>Quantity</th>
            </tr>
            {productBranchList.length > 0 &&
              productBranchList.map((element, index) => (
                <tr key={element['id']}>
                  <td>{element['id']}</td>
                  <td>{element['branchId']}</td>
                  <td>{element['productId']}</td>
                  <td>{element['quantity']}</td>
                  <td className='Transparent'>
                    <button
                      className='mx-1 btn Edit-btn'
                      onClick={() => {
                        this.onEditProductBranch(index);
                      }}
                    >
                      EDIT
                    </button>
                  </td>
                  <td className='Transparent'>
                    <button
                      className='mx-1 btn Delete-btn'
                      onClick={() => {
                        this.onDeleteProductBranch(index, element['id']);
                      }}
                    >
                      DELETE
                    </button>
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
      </>
    );
  }
}

export default ViewProductBranch;
