import React, { Component } from 'react';
import './style.css';
import * as API from '../../services/api/api';
class AddProductToBranch extends Component {
  state = {
    currentProductBranch: {}
  };

  postAddProductToBranch = async () => {
    if (
      !this.state.currentProductBranch.hasOwnProperty('branchId') ||
      !this.state.currentProductBranch.hasOwnProperty('productId') ||
      !this.state.currentProductBranch.hasOwnProperty('quantity') ||
      (this.state.currentProductBranch.hasOwnProperty('branchId') &&
        this.state.currentProductBranch['branchId'] === '') ||
      (this.state.currentProductBranch.hasOwnProperty('productId') &&
        this.state.currentProductBranch['productId'] === '') ||
      (this.state.currentProductBranch.hasOwnProperty('quantity') &&
        this.state.currentProductBranch['quantity'] === '')
    ) {
      alert(`Warning: some of the fields are empty, please check again!`);
      return;
    }

    const data = await API.fetchPostAddProductToBranch(
      this.state.currentProductBranch
    );
    if (data.status === 200) {
      alert(`successfully added!`);
      this.setState({
        ...this.state,
        currentProductBranch: { branchId: '', productId: '', quantity: '' }
      });
    } else {
      alert(
        `Unable to add because the same branch id and product id might already exist in the database!`
      );
    }
  };

  render() {
    return (
      <>
        <h1 className='HeaderAdd'>Add Product to Branch</h1>

        <div className='Custom_Container'>
          <div className='row'>
            <div className='col-3'>
              <label>Branch Id: </label>
            </div>
            <div className='col-6'>
              <input
                type='text'
                name='branch_id'
                value={this.state.currentProductBranch.branchId || ''}
                onChange={event => {
                  this.setState({
                    ...this.state,
                    currentProductBranch: {
                      ...this.state.currentProductBranch,
                      branchId: event.target.value
                    }
                  });
                }}
              />
            </div>
          </div>
          <div className='row'>
            <div className='col-3'>
              <label>Product Id: </label>
            </div>
            <div className='col-6'>
              <input
                type='text'
                name='product_id'
                value={this.state.currentProductBranch.productId || ''}
                onChange={event => {
                  this.setState({
                    ...this.state,
                    currentProductBranch: {
                      ...this.state.currentProductBranch,
                      productId: event.target.value
                    }
                  });
                }}
              />
            </div>
          </div>
          <div className='row'>
            <div className='col-3'>
              <label>Quantity: </label>
            </div>
            <div className='col-6'>
              <input
                type='number'
                name='quantity'
                value={this.state.currentProductBranch.quantity || ''}
                onChange={event => {
                  this.setState({
                    ...this.state,
                    currentProductBranch: {
                      ...this.state.currentProductBranch,
                      quantity: event.target.value
                    }
                  });
                }}
              />
            </div>
          </div>

          <button
            className='btn btn-primary small-btn'
            type='button'
            onClick={this.postAddProductToBranch}
          >
            Submit
          </button>
        </div>
      </>
    );
  }
}

export default AddProductToBranch;
