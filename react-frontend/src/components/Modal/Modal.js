import React, { Component } from 'react';
import Modal from 'react-modal';

const customStyles = {
  overlay: {
    position: 'fixed',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(255, 255, 255, 0.75)'
  },
  content: {
    top: '50%',
    left: '50%',
    right: '50%',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    borderRadius: '29px',
    backgroundImage: 'linear-gradient(90deg, #fcb, #f7c143)',
    borderColor: 'orange'
  }
};
Modal.setAppElement(document.getElementById('root'));

class CustomModal extends Component {
  render() {
    const { onRequestClose, isOpen } = this.props;
    return (
      <Modal
        style={customStyles}
        isOpen={isOpen}
        onRequestClose={onRequestClose}
        {...this.props}
      />
    );
  }
}

export default CustomModal;
