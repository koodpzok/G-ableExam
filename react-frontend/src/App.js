import React, { Component } from 'react';
import Main from './pages/Main';
import { inject, observer } from 'mobx-react';
class App extends React.Component {
  render() {
    return <Main />;
  }
}

export default inject('myStore')(observer(App));
