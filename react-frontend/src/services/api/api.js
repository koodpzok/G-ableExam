import React from 'react';
import * as API from '../config/config';

const { serverBaseUrl } = API;
//GET
function fetchGetProductList() {
  return fetch(serverBaseUrl + '/viewProduct');
}

function fetchGetBranchList() {
  return fetch(serverBaseUrl + '/viewBranch');
}

function fetchGetProductBranchList() {
  return fetch(serverBaseUrl + '/viewProductBranch');
}

//POST
function fetchPostAddProduct(data) {
  return fetch(serverBaseUrl + '/addProduct', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(data)
  });
}

function fetchPostAddBranch(data) {
  return fetch(serverBaseUrl + '/addBranch', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(data)
  });
}
function fetchPostAddProductToBranch(data) {
  return fetch(serverBaseUrl + '/addProductToBranch', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(data)
  });
}

//PUT
function fetchPutProduct(data, productId) {
  return fetch(serverBaseUrl + '/product/' + productId, {
    method: 'PUT',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(data)
  });
}

function fetchPutBranch(data, branchId) {
  return fetch(serverBaseUrl + '/branch/' + branchId, {
    method: 'PUT',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(data)
  });
}

function fetchPutProductBranch(data, id) {
  return fetch(serverBaseUrl + '/productBranch/' + id, {
    method: 'PUT',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(data)
  });
}

//DELETE
function fetchDeleteProduct(productId) {
  return fetch(serverBaseUrl + '/product/' + productId, {
    method: 'DELETE',
    headers: { 'Content-Type': 'application/json' }
  });
}
function fetchDeleteBranch(branchId) {
  return fetch(serverBaseUrl + '/branch/' + branchId, {
    method: 'DELETE',
    headers: { 'Content-Type': 'application/json' }
  });
}
function fetchDeleteBranchProduct(id) {
  return fetch(serverBaseUrl + '/productBranch/' + id, {
    method: 'DELETE',
    headers: { 'Content-Type': 'application/json' }
  });
}

export {
  fetchGetProductList,
  fetchGetBranchList,
  fetchGetProductBranchList,
  fetchPostAddProduct,
  fetchPostAddBranch,
  fetchPostAddProductToBranch,
  fetchPutProduct,
  fetchPutBranch,
  fetchPutProductBranch,
  fetchDeleteProduct,
  fetchDeleteBranch,
  fetchDeleteBranchProduct
};
