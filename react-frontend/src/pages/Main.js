import React from 'react';
import './Main.css';
import NavBar from '../components/NavBar/NavBar';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
//All pages for content show
import AddProduct from './Add/AddProduct';
import AddBranch from './Add/AddBranch';
import AddProductToBranch from './Add/AddProductToBranch';
import ViewProduct from './View/ViewProduct';
import ViewBranch from './View/ViewBranch';
import ViewProductBranch from './View/ViewProductBranch';

function Main() {
  return (
    <div>
      <Router>
        <header className='Header-Main'>Main Page</header>
        <hr className='Divider' />
        <NavBar />
        <div className='Header-Content'>Content</div>
        {/* content below */}
        <div className='container-fluid'>
          <Switch>
            <Route path='/addProduct' component={AddProduct} />
            <Route path='/addBranch' component={AddBranch} />
            <Route path='/addProductToBranch' component={AddProductToBranch} />
            <Route path='/viewProduct' exact component={ViewProduct} />
            <Route path='/viewBranch' exact component={ViewBranch} />
            <Route
              path='/viewProductBranch'
              exact
              component={ViewProductBranch}
            />
          </Switch>
        </div>
      </Router>
    </div>
  );
}

export default Main;
