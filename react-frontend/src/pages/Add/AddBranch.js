import React, { Component } from 'react';
import './style.css';
import * as API from '../../services/api/api';
class AddBranch extends Component {
  state = {
    currentBranch: {}
  };

  postAddBranch = async () => {
    if (
      !this.state.currentBranch.hasOwnProperty('name') ||
      !this.state.currentBranch.hasOwnProperty('address') ||
      (this.state.currentBranch.hasOwnProperty('name') &&
        this.state.currentBranch['name'] === '') ||
      (this.state.currentBranch.hasOwnProperty('address') &&
        this.state.currentBranch['address'] === '')
    ) {
      alert(`Warning: some of the fields are empty, please check again!`);
      return;
    }

    const data = await API.fetchPostAddBranch(this.state.currentBranch);
    if (data.status === 200) {
      alert(`successfully added!`);
      this.setState({
        ...this.state,
        currentBranch: { name: '', address: '' }
      });
    }
  };

  render() {
    return (
      <>
        <h1 className='HeaderAdd'>Add Branch</h1>

        <div className='Custom_Container'>
          <div className='row'>
            <div className='col-3'>
              <label>Branch name: </label>
            </div>
            <div className='col-6'>
              <input
                type='text'
                name='branch_name'
                value={this.state.currentBranch.name || ''}
                onChange={event => {
                  this.setState({
                    ...this.state,
                    currentBranch: {
                      ...this.state.currentBranch,
                      name: event.target.value
                    }
                  });
                }}
              />
            </div>
          </div>
          <div className='row'>
            <div className='col-3'>
              <label>Address: </label>
            </div>
            <div className='col-6'>
              <input
                type='text'
                name='address'
                value={this.state.currentBranch.address || ''}
                onChange={event => {
                  this.setState({
                    ...this.state,
                    currentBranch: {
                      ...this.state.currentBranch,
                      address: event.target.value
                    }
                  });
                }}
              />
            </div>
          </div>

          <button
            className='btn small-btn'
            type='button'
            onClick={this.postAddBranch}
          >
            Submit
          </button>
        </div>
      </>
    );
  }
}

export default AddBranch;
