import React, { Component } from 'react';
import './style.css';
import * as API from '../../services/api/api';

class AddProduct extends Component {
  state = {
    currentProduct: {}
  };

  postAddProduct = async () => {
    //detail is nullable
    if (
      !this.state.currentProduct.hasOwnProperty('name') ||
      !this.state.currentProduct.hasOwnProperty('price') ||
      (this.state.currentProduct.hasOwnProperty('name') &&
        this.state.currentProduct['name'] === '') ||
      (this.state.currentProduct.hasOwnProperty('price') &&
        this.state.currentProduct['price'] === '')
    ) {
      alert(`Warning: some of the fields are empty, please check again!`);
      return;
    }

    const data = await API.fetchPostAddProduct(this.state.currentProduct);
    if (data.status === 200) {
      alert(`successfully added!`);
      this.setState({
        ...this.state,
        currentProduct: { name: '', detail: '', price: '' }
      });
    }
  };

  render() {
    return (
      <>
        <h1 className='HeaderAdd'>Add Product</h1>

        <div className='Custom_Container'>
          <div className='row'>
            <div className='col-3'>
              <label>Product name: </label>
            </div>
            <div className='col-6'>
              <input
                type='text'
                name='product_name'
                value={this.state.currentProduct.name || ''}
                onChange={event => {
                  this.setState({
                    ...this.state,
                    currentProduct: {
                      ...this.state.currentProduct,
                      name: event.target.value
                    }
                  });
                }}
              />
            </div>
          </div>
          <div className='row'>
            <div className='col-3'>
              <label>Detail: </label>
            </div>
            <div className='col-6'>
              <input
                type='text'
                name='detail'
                value={this.state.currentProduct.detail || ''}
                onChange={event => {
                  this.setState({
                    ...this.state,
                    currentProduct: {
                      ...this.state.currentProduct,
                      detail: event.target.value
                    }
                  });
                }}
              />
            </div>
          </div>
          <div className='row'>
            <div className='col-3'>
              <label>Price: </label>
            </div>
            <div className='col-6'>
              <input
                type='number'
                step='.01'
                name='price'
                value={this.state.currentProduct.price || ''}
                onChange={event => {
                  this.setState({
                    ...this.state,
                    currentProduct: {
                      ...this.state.currentProduct,
                      price: event.target.value
                    }
                  });
                }}
              />
            </div>
          </div>

          <button
            className='btn btn-primary small-btn'
            type='button'
            onClick={this.postAddProduct}
          >
            Submit
          </button>
        </div>
      </>
    );
  }
}

export default AddProduct;
