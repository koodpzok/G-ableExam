// POST
package com.g_able.exam.spring.template.app.controller;

import com.g_able.exam.spring.template.app.service.ProductService;
import com.g_able.exam.spring.template.app.service.BranchService;
import com.g_able.exam.spring.template.app.service.BranchProductService;

import com.g_able.exam.spring.template.app.entity.Product;
import com.g_able.exam.spring.template.app.entity.Branch;
import com.g_able.exam.spring.template.app.entity.BranchProduct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class AddController {
    @Autowired
    private ProductService productService;
    @Autowired
    private BranchService branchService;
    @Autowired
    private BranchProductService branchProductService;

    @PostMapping(path = "/addProduct", produces = "application/json")
    public ResponseEntity<Product> addProduct(@RequestBody Product p) {
        Product product = new Product(p.getName(), p.getDetail(), p.getPrice());
        productService.persistProduct(product);
        return new ResponseEntity<Product>(product, HttpStatus.OK);
    }

    @PostMapping(path = "/addBranch", produces = "application/json")
    public ResponseEntity<Branch> addBranch(@RequestBody Branch b) {
        Branch branch = new Branch(b.getName(), b.getAddress());
        branchService.persistBranch(branch);
        return new ResponseEntity<Branch>(branch, HttpStatus.OK);
    }

    @PostMapping(path = "/addProductToBranch", produces = "application/json")
    public ResponseEntity<BranchProduct> addBranch(@RequestBody BranchProduct bp) {
        BranchProduct branchProduct = new BranchProduct(bp.getBranchId(), bp.getProductId(), bp.getQuantity());

        boolean canPersist = branchProductService.persistBranchProduct(branchProduct);
        if (!canPersist) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<BranchProduct>(branchProduct, HttpStatus.OK);
    }
}