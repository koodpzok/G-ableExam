package com.g_able.exam.spring.template.app.compositekey;

import java.io.Serializable;

public class BranchProductCompositeKey implements Serializable {
    private static final long serialVersionUID = 1L;
    private long branchId;
    private long productId;

    public BranchProductCompositeKey() {
    }

    public BranchProductCompositeKey(long branchId, long productId) {

        this.branchId = branchId;
        this.productId = productId;
    }

    public long getBranchId() {
        return this.branchId;
    }

    public void setBranchId(long branchId) {
        this.branchId = branchId;
    }

    public long getProductId() {
        return this.productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

}
