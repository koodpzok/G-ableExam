import React, { Component } from 'react';
import { Link } from 'react-router-dom';
function Nav() {
  return (
    <nav>
      <ul>
        <Link to='/addProduct'>
          <li>Add Product</li>
        </Link>
        <Link to='/addBranch'>
          <li>Add Branch</li>
        </Link>
        <Link to='/addProductToBranch'>
          <li>Add Product to Branch</li>
        </Link>
      </ul>
      <hr className='Divider' />
      <ul>
        <Link to='/viewProduct'>
          <li>View list of products</li>
        </Link>
        <Link to='/viewBranch'>
          <li>View list of Branches</li>
        </Link>
        <Link to='/viewProductBranch'>
          <li>View list of products in each branch</li>
        </Link>
      </ul>
      <hr className='Divider' />
      <ul>
        <Link to='/'>
          <li>Clear Content</li>
        </Link>
      </ul>
    </nav>
  );
}

export default Nav;
